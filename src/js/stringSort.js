window.addEventListener("load", function () {

    var form = document.getElementById("phrase-form");

    form.addEventListener("submit", function (event) {
        event.preventDefault();

        var phrase = document.getElementById('phrase').value;

        /* ------------------------------------ */
        var radioMethod = document.getElementsByName('sortMethod');

        var sortMethod = function(){
            var len = radioMethod.length;
            for (var i=0; i < len; i++)
            {
                if (radioMethod[i].checked) {
                    return radioMethod[i].value;
                }
            }
        }
        /* ------------------------------------ */

        console.log("submit data");
        sortWords(phrase,sortMethod);
    });

});

function sortWords(phrase,sortMethod){

    console.log(phrase);

    var phraseArr = phrase.split(" ");
    var answer = document.getElementById('answer');

    if(sortMethod() == 'alpha'){
        phraseArr.sort();

        answer.innerHTML = '<ul>';

        for(var i = 0; i < phraseArr.length; i++ ){
            answer.innerHTML += '<li>' + phraseArr[i] + '</li>';
        }

        answer.innerHTML += '</ul>';

    }else{

        phraseArr.sort(function(a, b){
            // ASC  -> a.length - b.length
            // DESC -> b.length - a.length
            return a.length - b.length;
        });

        answer.innerHTML = '<ul>';

        for(var i = 0; i < phraseArr.length; i++ ){
            answer.innerHTML += '<li>' + phraseArr[i] + '</li>';
        }

        answer.innerHTML += '</ul>';
    }

}