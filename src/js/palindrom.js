var palindromeFrom = document.getElementById('palindromeFrom');

palindromeFrom.onsubmit = function(){

    hideAlert();

    var inputWord = document.getElementById('word').value;
    var palindromeAnswer = document.getElementById('palindromeAnswer');

    if(inputWord.length == 0){

        showAlert("formularz nie może być pusty");
        return false;

    }

    if(checkIsPalindrome(inputWord)){
        // palindrom
        palindromeAnswer.innerHTML = "Super! "+ inputWord +" jest palindromem";
    }else{
        // nie palindrom
        palindromeAnswer.innerHTML = "Niestety... "+ inputWord +" to nie palindrom";
    };

    return false;
};

function checkIsPalindrome(inputWord){

    var word = inputWord.trim().replace(/\s/g, '');
    var wordReversed = word.split('').reverse().join('');

    if(word === wordReversed){
        //console.log("to jest palindrom");
        return true;
    }else{
        //console.log("to nie jest palindrom");
        return false;
    }

};

function showAlert(alertMessage){

    document.getElementById('palindromeAlert').style.display = "block";
    document.getElementById('palindromeAlert').innerHTML = alertMessage;

}

function hideAlert(){
    document.getElementById('palindromeAlert').style.display = "none";
}