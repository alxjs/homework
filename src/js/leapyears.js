function getLeapYears(min, max){

    var leapYears = [];

    while (max >= min) {

        if((min % 4 == 0 && min % 100 != 0) || (min % 400 == 0)){
            leapYears.push(min);
        }

        min++;
    }

    return leapYears;

}

//console.log(getLeapYears(1968,2018));